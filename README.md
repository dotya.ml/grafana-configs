# [grafana-configs](https://git.dotya.ml/dotya.ml/grafana-configs)

this repo contains configuration files of [our grafana
instance](https://grafana.dotya.ml), including the `systemd` unit files and the
`nebula configs` dashboard

### TO DO
- [ ] automated deployment (preferably using ansible + drone)

### LICENSE
WTFPLv2, see [LICENSE](LICENSE) for details
