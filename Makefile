# ref:
#	https://community.grafana.com/t/ngalert-grafana-8-alert-feature-how-to-export-import-alerts-as-yml-json/51677/22

CHECK-GRAFANA-TOKEN:
ifndef GRAFANA_TOKEN
	$(error GRAFANA_TOKEN is required)
endif

DOWNLOAD-GRAFANA-ALERTS: CHECK-GRAFANA-TOKEN
	curl -X GET \
		-H "Authorization: Bearer ${GRAFANA_TOKEN}" \
		'https://grafana.dotya.ml/api/ruler/grafana/api/v1/rules' \
		| jq > './alerts/alerts.json'

UPLOAD-GRAFANA-ALERTS: CHECK-GRAFANA-TOKEN
	./upload-grafana-alerts.sh
