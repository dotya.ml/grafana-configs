#!/bin/bash

ALERTS_JSON_PATH=./alerts/alerts.json
NUMBER_OF_ALERTS=$(jq -c '.["folder-name"] | length' ${ALERTS_JSON_PATH})

for ((i=0; i<NUMBER_OF_ALERTS; i++)); do
  ALERT_OBJECT=$(jq -c --arg i "$i" '.["folder-name"][($i | tonumber)] | del(.rules[0].grafana_alert.uid)' ${ALERTS_JSON_PATH})
  ALERT_NAME=$(jq -c --arg i "$i" '.["folder-name"][($i | tonumber)].name' ${ALERTS_JSON_PATH})
  printf "Creating %s..." "${ALERT_NAME}"
	curl -X POST \
		-H "Authorization: Bearer ${GRAFANA_TOKEN}" \
		-H "Content-type: application/json" \
		'https://grafana.dotya.ml/api/ruler/grafana/api/v1/rules/folder-name' \
		-d "${ALERT_OBJECT}"
	printf "\n"
done
